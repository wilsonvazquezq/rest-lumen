<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Empresa ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function resource($uri, $controller, $router)
{
    //$verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];
    $router->get($uri, $controller.'@index');
    $router->post($uri.'/' ,$controller.'@create');
    $router->put($uri.'/' ,$controller.'@update');
    
    $router->get($uri.'/{id}', $controller.'@get');
    $router->delete($uri.'/{id}', $controller.'@delete');

    $router-> post($uri.'/img',  $controller.'@saveFile'); 
//    $router-> post ($uri,  $controller.'@getFileList'); 
    $router-> post ($uri.'/vimg', $controller.'@viewFile');
}
resource('api/empresa', 'EmpresaController', $router);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Historial ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function  Historial($uri, $controller, $router)
{
    //$verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];
    $router->get($uri, $controller.'@index');
    $router->post($uri.'/' ,$controller.'@create');
    $router->put($uri.'/' ,$controller.'@update');
    
    $router->get($uri.'/{id}', $controller.'@get');
    $router->delete($uri.'/{id}', $controller.'@delete');
}
Historial('api/historialpedido', 'HistorialController', $router);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Stock ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function  Stock($uri, $controller, $router)
{
    //$verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];
    $router->get($uri, $controller.'@index');
    $router->post($uri.'/' ,$controller.'@create');
    $router->put($uri.'/' ,$controller.'@update');
    
    $router->get($uri.'/{id}', $controller.'@get');
    $router->delete($uri.'/{id}', $controller.'@delete');
}
Stock('api/stock', 'StockController', $router);
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Usuario ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function  Usuario($uri, $controller, $router)
{
    //$verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];
    $router->get($uri, $controller.'@index');
    $router->post($uri.'/' ,$controller.'@create');
    $router->put($uri.'/' ,$controller.'@update');
    
    $router->get($uri.'/{id}', $controller.'@get');
    $router->delete($uri.'/{id}', $controller.'@delete');
}
Usuario('api/usuario', 'UsuarioController', $router);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Categoria ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function  Categoria($uri, $controller, $router)
{
    //$verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];
   
    $router->get($uri, $controller.'@index');
    $router->post($uri.'/' ,$controller.'@create');
    $router->put($uri.'/' ,$controller.'@update');

    $router->get($uri.'/{id}', $controller.'@get');
    $router->delete($uri.'/{id}', $controller.'@delete');

    $router-> post($uri.'/img',  $controller.'@saveFile'); 
//    $router-> post ($uri,  $controller.'@getFileList'); 
    $router-> post ($uri.'/vimg', $controller.'@viewFile');


}
Categoria('api/categoria', 'CategoriaController', $router);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Bodega ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function  Bodega($uri, $controller, $router)
{
    //$verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];
   
    $router->get($uri, $controller.'@index');
    $router->post($uri.'/' ,$controller.'@create');
    $router->put($uri.'/' ,$controller.'@update');

    $router->get($uri.'/{idBodega}', $controller.'@get');
    $router->delete($uri.'/{idBodega}', $controller.'@delete');

}
Bodega('api/bodega', 'BodegaController', $router);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Perfil ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function Perfil($uri, $controller, $router)
{
    //$verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];
    $router->get($uri, $controller.'@index');
    //$router->get($uri, $controller.'@indexxx');
    $router->post($uri ,$controller.'@create');
    $router->put($uri ,$controller.'@update');
    
    $router->get($uri.'/{id}', $controller.'@get');
    $router->delete($uri.'/{id}', $controller.'@delete');
}
Perfil('api/perfil', 'PerfilController', $router);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Direccion ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function  Direccion($uri, $controller, $router)
{
    //$verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];
    $router->get($uri, $controller.'@index');
    $router->post($uri.'/' ,$controller.'@create');
    $router->put($uri.'/' ,$controller.'@update');
    
    $router->get($uri.'/{id}', $controller.'@get');
    $router->delete($uri.'/{id}', $controller.'@delete');
}
Direccion('api/direccion', 'DireccionController', $router);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Persona ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function Persona($uri, $controller, $router)
{
    //$verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];
    $router->get($uri, $controller.'@index');
    $router->post($uri.'/' ,$controller.'@create');
    $router->put($uri.'/' ,$controller.'@update');
    
    $router->get($uri.'/{id}', $controller.'@get');
    $router->delete($uri.'/{id}', $controller.'@delete');

    $router-> post($uri.'/img',  $controller.'@saveFile'); 
//    $router-> post ($uri,  $controller.'@getFileList'); 
    $router-> post ($uri.'/vimg', $controller.'@viewFile');
}
Persona('api/persona', 'PersonaController', $router);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Producto ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function Producto($uri, $controller, $router)
{
    //$verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];
    $router->get($uri, $controller.'@index');
    $router->post($uri.'/' ,$controller.'@create');
    $router->put($uri.'/' ,$controller.'@update');
    
    $router->get($uri.'/{id}', $controller.'@get');
    $router->delete($uri.'/{id}', $controller.'@delete');

    $router-> post($uri.'/img',  $controller.'@saveFile'); 
//    $router-> post ($uri,  $controller.'@getFileList'); 
    $router-> post ($uri.'/vimg', $controller.'@viewFile'); 
//     $router-> get ($uri, '/{imagen}', $controller.'@deleteFile');

 }
Producto('api/producto', 'ProductoController', $router);

// $app-> post('file','\App\Http\Controllers\ProductoController@saveFile'); 
// $app-> post ('list','\App\Http\Controllers\ProductoController@getFileList'); 
// $app-> get ('view/{filename}','\App\Http\Controllers\ProductoController@viewFile'); 
// $app-> get ('delete/{filename}','\App\Http\Controllers\ProductoController@deleteFile');



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Pedido ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function Pedido($uri, $controller, $router)
{
    //$verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];
    $router->get($uri, $controller.'@index');
    $router->post($uri.'/' ,$controller.'@create');
    $router->put($uri.'/' ,$controller.'@update');
    
    $router->get($uri.'/{id}', $controller.'@get');
    $router->delete($uri.'/{id}', $controller.'@delete');
}
Pedido('api/pedido', 'PedidoController', $router);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ DetallePedido ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

function DetallePedido($uri, $controller, $router)
{
    //$verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];
    $router->get($uri, $controller.'@index');
    $router->post($uri.'/' ,$controller.'@create');
    $router->put($uri.'/' ,$controller.'@update');
    
    $router->get($uri.'/{id}', $controller.'@get');
    $router->delete($uri.'/{id}', $controller.'@delete');
}
DetallePedido('api/detallepedido', 'DetallePedidoController', $router);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Detalleproducto ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function DetalleProducto($uri, $controller, $router)
{
    //$verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];
    $router->get($uri, $controller.'@index');
    $router->post($uri.'/' ,$controller.'@create');
    $router->put($uri.'/' ,$controller.'@update');
    
    $router->get($uri.'/{id}', $controller.'@get');
    $router->delete($uri.'/{id}', $controller.'@delete');
}
DetalleProducto('api/detalleproducto', 'DetalleProductoController', $router);


