<?php

namespace App;
use App\Direccion;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = ['nombre', 'telefono','imagen', 'idDireccion'];
    //
    protected $table = 'empresa';


    public function bodegas()
    {
        return $this->hasOne('App\bodega');
    }

}
