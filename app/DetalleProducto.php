<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleProducto extends Model
{
    protected $fillable = ['material', 'descripcion', 'caducidad'];
    //
    protected $table = 'detalleproducto';
}
