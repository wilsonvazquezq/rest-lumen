<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    protected $fillable = ['ciudad','callePrincipal', 'calleSecundaria', 'numeroCasa', 'codigoPostal'];
    //
    protected $table = 'direccion';
}

