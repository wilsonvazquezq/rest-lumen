<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $fillable = ['usuario', 'password','idperfil'];
    //
    protected $table = 'usuario';


    public function perfiles()
    {
        return $this->hasMany('App\perfil');
    }
}
