<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bodega extends Model
{
    protected $fillable = ['nombre']; //only the field names inside the array can be mass-assign

    //
    protected $table = 'bodega';

    public function empresas()
    {
        return $this->hasMany('App\empresa');
    }

    public function stocks()
    {
        return $this->hasOne('App\stock');
    }
}
