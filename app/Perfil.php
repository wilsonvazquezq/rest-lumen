<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    //
    protected $fillable = ['tipo'];
    protected $table = 'perfil';

    public function usuarios()
    {
        return $this->hasOne('App\usuario');
    }
}
