<?php

namespace App\Http\Controllers;
use App\Persona;

use Illuminate\Http\Request;
use mysql_xdevapi\Exception;
use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

//use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;

class PersonaController extends Controller
{
    public function __construct(\App\Persona $persona)


    {
        $this->persona = $persona;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * @OA\Get(
     *     path="/rest-lumen/public/api/persona",
     *     tags={"persona"},

     *     @OA\Response(response="200", description="An example resource")
     * )
     */
    public function index(\App\Persona $persona,Request $request)
    {

      //  return $this->API_KEY;
        $queryStrings = $request->except(['limit', 'order_by', 'order', 'page', 'count', 'current_page', 'last_page', 'next_page_url', 'per_page', 'previous_page_url', 'total', 'url', 'from', 'to']);

        $limit = ($request->input('limit') ? $request->input('limit') : '10');
        $order_by = ($request->input('order') ? $request->input('order') : 'id');
        $order = ($request->input('order_by') ? $request->input('order_by') : 'desc');
        $page = ($request->input('page') ? $request->input('page') : '1');

        if($limit >= 100) {
            $limit = 100;
        }
        $query = DB::table('persona');

        foreach ($queryStrings as $key => $value) {
            if($key=='filter'){
                $query->where('nombre', 'like',  '%'.$value.'%');
               // $query->where('direccion', '=',  $value);

            }else{
                $query->where($key, '=',  $value);
            }

        }

        $query->orderBy($order_by, $order);
        $data= $query->paginate($limit);

//        $data = array();
//        $data = $query->get();

        return response()->json( $data);

        //return $persona->paginate(10);
    }

    public function todos(\App\Persona $persona)
    {
        return response()->json(Persona::all());
    }
    /**
     * @OA\Get(
     *     path="/rest-lumen/public/api/persona/{id}",
     *     summary="Buscar Persona por id",
     *     description="Retorna una persona",
     *     operationId="recuperar por Id",
     *     tags={"persona"},
     *     @OA\Parameter(
     *         description="ID of pet to return",
     *         in="path",
     *         name="petId",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid ID supplied"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Pet not found"
     *     ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */
    public function get($id)
    {
        return response()->json(Persona::find($id));
    }

    public function create(Request $request)
    {
        $data = ($request->json()->all());
        try {
            if (array_key_exists("id",$data)){
                $author = Persona::findOrFail($data['id']);
                $author->update($request->all());

                return response()->json($author, 201);
            }else{
                $author = Persona::create($data);

                return response()->json($author, 201);
            }

        } catch (Exception $exceptione) {
            return $exceptione;
        }

    }

    public function update( Request $request)
    {
        //return test;
        $data = ($request->json()->all());
        try {
            $author = Persona::findOrFail($data['id']);
            $author->update($request->all());

            return response()->json($author, 201);
        } catch (Exception $exceptione) {
            return $exceptione;
        }
    }

    public function delete($id)
    {
        Persona::findOrFail($id)->delete();
        return response('', 200);
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++ metodod para cargar las images+++++++++++++++++++++++++++++++++
public function saveFile(Request $request )
{
// $file = Request::file('file');
$file = $request->file('file');
Storage::put($file->getClientOriginalName(), File::get($file));

$resp=['nombre' => $file->getClientOriginalName()];
return response()->json($resp);
}
public function deleteFile($imagen)
{
Storage::delete($imagen);
return response()->json('success');
}
public function getFileList(){
$files = Storage::files('/');
return response()->json($files);
}
public function viewFile(Request $request){
   $imagen = $request->nombre;
$path = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.$imagen;
return response()->make(file_get_contents($path), 200, [
'Content-Type' => Storage::mimeType($imagen),
'Content-Disposition' => 'inline; '.$imagen, ]);
}

}
