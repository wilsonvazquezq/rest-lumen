<?php

namespace App\Http\Controllers;

use App\Historial;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;
use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Facades\DB;

class HistorialController extends Controller
{
    public function __construct(\App\Historial $historial)


    {
        $this->historial = $historial;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * @OA\Get(
     *     path="/rest-lumen/public/api/historial",
     *     tags={"historial"},

     *     @OA\Response(response="200", description="An example resource")
     * )
     */
    public function index(\App\Historial $historial,Request $request)
    {

      //  return $this->API_KEY;
        $queryStrings = $request->except(['limit', 'order_by', 'order', 'page', 'count', 'current_page', 'last_page', 'next_page_url', 'per_page', 'previous_page_url', 'total', 'url', 'from', 'to']);

        $limit = ($request->input('limit') ? $request->input('limit') : '10');
        $order_by = ($request->input('order') ? $request->input('order') : 'id');
        $order = ($request->input('order_by') ? $request->input('order_by') : 'desc');
        $page = ($request->input('page') ? $request->input('page') : '1');

        if($limit >= 100) {
            $limit = 100;
        }
        $query = DB::table('historialPedido');

        foreach ($queryStrings as $key => $value) {
            if($key=='filter'){
                $query->where('nombre', 'like',  '%'.$value.'%');
               // $query->where('direccion', '=',  $value);

            }else{
                $query->where($key, '=',  $value);
            }

        }

        $query->orderBy($order_by, $order);
        $data= $query->paginate($limit);

//        $data = array();
//        $data = $query->get();

        return response()->json( $data);

        //return $historial->paginate(10);
    }

    public function todos(\App\Historial $historial)
    {
        return response()->json(Historial::all());
    }
    /**
     * @OA\Get(
     *     path="/rest-lumen/public/api/historial/{id}",
     *     summary="Buscar Historial por id",
     *     description="Retorna una historial",
     *     operationId="recuperar por Id",
     *     tags={"historial"},
     *     @OA\Parameter(
     *         description="ID of pet to return",
     *         in="path",
     *         name="petId",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid ID supplied"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Pet not found"
     *     ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */
    public function get($id)
    {
        return response()->json(Historial::find($id));
    }

    public function create(Request $request)
    {
        $data = ($request->json()->all());
        try {
            if (array_key_exists("id",$data)){
                $author = Historial::findOrFail($data['id']);
                $author->update($request->all());

                return response()->json($author, 201);
            }else{
                $author = Historial::create($data);

                return response()->json($author, 201);
            }

        } catch (Exception $exceptione) {
            return $exceptione;
        }

    }

    public function update( Request $request)
    {
        //return test;
        $data = ($request->json()->all());
        try {
            $author = Historial::findOrFail($data['id']);
            $author->update($request->all());

            return response()->json($author, 201);
        } catch (Exception $exceptione) {
            return $exceptione;
        }
    }

    public function delete($id)
    {
        Historial::findOrFail($id)->delete();
        return response('', 200);
    }
}
