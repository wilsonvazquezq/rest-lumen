<?php

namespace App\Http\Controllers;

use App\Bodega;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;
use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Facades\DB;


class BodegaController extends Controller
{

    public function __construct(\App\Bodega $bodega)


    {
        $this->bodega = $bodega;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * @OA\Get(
     *     path="/rest-lumen/public/api/bodega",
     *     tags={"bodega"},

     *     @OA\Response(response="200", description="An example resource")
     * )
     */
    public function index(\App\Bodega $bodega,Request $request)
    {

      //  return $this->API_KEY;
        $queryStrings = $request->except(['limit', 'order_by', 'order', 'page', 'count', 'current_page', 'last_page', 'next_page_url', 'per_page', 'previous_page_url', 'total', 'url', 'from', 'to']);

        $limit = ($request->input('limit') ? $request->input('limit') : '10');
        $order_by = ($request->input('order') ? $request->input('order') : 'id');
        $order = ($request->input('order_by') ? $request->input('order_by') : 'desc');
        $page = ($request->input('page') ? $request->input('page') : '1');

        if($limit >= 100) {
            $limit = 100;
        }
        $query = DB::table('bodega');

        foreach ($queryStrings as $key => $value) {
            if($key=='filter'){
                $query->where('nombre', 'like',  '%'.$value.'%');
               // $query->where('direccion', '=',  $value);

            }else{
                $query->where($key, '=',  $value);
            }

        }

        $query->orderBy($order_by, $order);
        $data= $query->paginate($limit);

//        $data = array();
//        $data = $query->get();

        return response()->json( $data);

        //return $bodega->paginate(10);
    }

    public function todos(\App\Bodega $bodega)
    {
        return response()->json(Bodega::all());
    }
    /**
     * @OA\Get(
     *     path="/rest-lumen/public/api/bodega/{id}",
     *     summary="Buscar Bodega por id",
     *     description="Retorna una bodega",
     *     operationId="recuperar por Id",
     *     tags={"bodega"},
     *     @OA\Parameter(
     *         description="ID of pet to return",
     *         in="path",
     *         name="petId",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid ID supplied"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Pet not found"
     *     ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */
    public function get($id)
    {
        return response()->json(Bodega::find($id));
    }

    public function create(Request $request)
    {
        $data = ($request->json()->all());
        try {
            if (array_key_exists("id",$data)){
                $author = Bodega::findOrFail($data['id']);
                $author->update($request->all());

                return response()->json($author, 201);
            }else{
                $author = Bodega::create($data);

                return response()->json($author, 201);
            }

        } catch (Exception $exceptione) {
            return $exceptione;
        }

    }

    public function update( Request $request)
    {
        //return test;
        $data = ($request->json()->all());
        try {
            $author = Bodega::findOrFail($data['id']);
            $author->update($request->all());

            return response()->json($author, 201);
        } catch (Exception $exceptione) {
            return $exceptione;
        }
    }

    public function delete($id)
    {
        Bodega::findOrFail($id)->delete();
        return response('', 200);
    }
}
