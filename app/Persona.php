<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $fillable = ['cedula', 'nombre', 'apellido','fechaNacimiento','email', 'telefono', 'estado', 'imagen'];
    //
    protected $table = 'persona';
    
}
