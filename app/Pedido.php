<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $fillable = ['descripcion', 'fechaPedido', 'fechaEntrega', 'subTotal', 'iva', 'precioEnvio'. 'total', 'estado'];
    //
    protected $table = 'pedido';
    
}
