<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = ['cantidad'];
    //
    protected $table = 'stock';

    public function bodegas()
    {
        return $this->hasMany('App\bodega');
    }
}
