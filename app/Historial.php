<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historial extends Model
{
    protected $fillable = ['lugarDestino', 'lugarFinal', 'tiempoEstimado', 'fecha'];
    //
    protected $table = 'historialpedido';
}
