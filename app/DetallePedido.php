<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetallePedido extends Model
{
    protected $fillable = ['cantidad'];
    //
    protected $table = 'detallePedido';
    
}
