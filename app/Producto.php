<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable = ['nombre', 'descripcion', 'precio', 'imagen1','imagen2', 'imgen3'];
    //
    protected $table = 'producto';
    
}
